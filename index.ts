import {GenericContainer} from "testcontainers";
import http, {IncomingMessage, RequestOptions} from "http";

console.log("Starting container");

const container = await new GenericContainer("alexanderp2/geth2:test")
    .withExposedPorts(8545)
    .withCommand([
        "--http",
        "--http.addr=0.0.0.0",
        "--http.corsdomain=*",
        "--http.vhosts=*",
        "--dev",
    ])
    .start();

const rpcHttp = `http://${container.getHost()}:${container.getMappedPort(8545)}`;

console.log("Container started, can be accessed on", rpcHttp);

function doRequest(data: string): Promise<unknown> {
    return new Promise((resolve, reject) => {

        const rpcHttpUrl = new URL(rpcHttp);

        const httpOptions: RequestOptions = {
            host: rpcHttpUrl.hostname,
            path: "/",
            port: rpcHttpUrl.port,
            method: "POST",
            headers: {"Content-Type": "application/json"},
        };


        const req = http.request(httpOptions, (res: IncomingMessage) => {
            res.setEncoding("utf8");
            let responseBody = "";

            res.on("data", (chunk: string) => {
                responseBody += chunk;
            });

            res.on("end", () => {
                resolve(JSON.parse(responseBody));
            });
        });

        req.on("error", (err: Error) => {
            reject(err);
        });

        req.write(data);
        req.end();
    });
}

const result = await doRequest(JSON.stringify({
    jsonrpc: "2.0",
    method: "eth_coinbase",
    params: [],
    id: 1,
}));

console.log("Get geth coinbase", result);

await container.stop();

console.log("Container stopped");

process.exit(0);

